Description
-----------
Module Implements off-site card payment services on Bulgarian Fibank (http://fibank.bg) gateway for the Drupal Commerce payment and checkout system.

Requirements
------------
Drupal 7.x
Commerce
Commerce
Commerce Payment
Commerce Payment UI
Commerce Checkout
Commerce Cart
Commerce Order UI
Rules

cURL for PHP - the Commerce Fibank module will check for proper cURL installation and report an error if corrective action is required. cURL is commonly available on standard LAMP-based server installations by default.
After installing the module, check the curl status at admin/reports/status.

Installation
------------
1. Copy the module directory in the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. Get official merchant account in Fibank.bg (Contact - Customer Relations and POS
Card Services Division)

To convert the certificate provided by the bank from a Java keystore file into a PEM file, use keytool and openssl applications.
keytool -importkeystore -srckeystore cert-file.jks -destkeystore cert-file.p12 -srcstoretype JKS -deststoretype PKCS12 -srcstorepass XXXXXX -deststorepass XXXXXX -srcalias cert-alias -destalias cert-alias
openssl pkcs12 -in cert-file.p12 -out cert-file.pem -passin pass:XXXXXX -passout pass:XXXXXX

Use 'commerce_fibank_cert' variable in settings.php to set up path to certificate and its password.
$conf['commerce_fibank_cert'] = array(
	'path' => 'path_to_sertificate.pem',
	'pass' => 'password',
);

Support
-------
Please use the issue queue for filing bugs with this module at

Author
------
Ivan Koychev
https://www.drupal.org/user/1062686
